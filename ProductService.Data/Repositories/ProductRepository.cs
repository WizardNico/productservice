﻿using Microsoft.EntityFrameworkCore;
using ProductService.Data.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductService.Data.Repositories
{
	public static class ProductRepository
	{
		public static async Task<Product> FindByCodeAsync(this DbSet<Product> repository, string code)
		{
			Product product = await repository
				.Include(prod => prod.Category)
				.Include(prod => prod.OptionProducts)
				.ThenInclude(opt => opt.Option)
				.Where(prod => prod.Code.ToLower() == code.ToLower())
				.SingleOrDefaultAsync();

			return (product);
		}

		public static async Task<Category> FindCategoryByCodeAsync(this DbSet<Category> repository, string code)
		{
			Category category = await repository
				.Where(cat => cat.Code.ToLower() == code.ToLower())
				.SingleOrDefaultAsync();

			return (category);
		}

		public static async Task<Option> FindOptionByCodeAsync(this DbSet<Option> repository, string code)
		{
			Option option = await repository
				.Where(opt => opt.Code.ToLower() == code.ToLower())
				.SingleOrDefaultAsync();

			return (option);
		}

		public static async Task<List<Product>> GetListAsync(this DbSet<Product> repository, string categoryCode, int pageSize, long pageIndex)
		{
			IQueryable<Product> query = repository
				.Include(prod => prod.Category)
				.Include(prod => prod.OptionProducts)
				.ThenInclude(opt => opt.Option)
				.Where(prod => prod.Category.Code.ToLower() == categoryCode.ToLower());

			List<Product> result = await query
				.Skip(pageSize * ((int)pageIndex - 1))
				.Take(pageSize).ToListAsync();

			return (result);
		}

		public static async Task<int> CountAsync(this DbSet<Product> repository, string categoryCode)
		{
			IQueryable<Product> query = repository
				.Include(prod => prod.Category)
				.Include(prod => prod.OptionProducts)
				.ThenInclude(opt => opt.Option)
				.Where(prod => prod.Category.Code.ToLower() == categoryCode.ToLower());

			//esecuzione della query di conteggio
			int result = await query
				.CountAsync();

			return (result);
		}
	}
}
