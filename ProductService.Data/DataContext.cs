﻿using Microsoft.EntityFrameworkCore;
using ProductService.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProductService.Data
{
	public class DataContext : DbContext
	{
		public DbSet<Category> Categories { get; set; }
		public DbSet<Option> Options { get; set; }
		public DbSet<OptionProduct> OptionProducts { get; set; }
		public DbSet<Product> Products { get; set; }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseSqlServer(@"Data Source=localhost;Initial Catalog=ProductService;Persist Security Info=True;User ID=sa;Password=080809xx;Pooling=False;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=True");
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<OptionProduct>()
				.HasIndex(op => new { op.OptionId, op.ProductId })
				.IsUnique(true);

			modelBuilder.Entity<Option>()
				.HasIndex(o => new { o.Code })
				.IsUnique(true);

			modelBuilder.Entity<Category>()
				.HasIndex(c => new { c.Code })
				.IsUnique(true);

			modelBuilder.Entity<Product>()
				.HasIndex(p => new { p.Code })
				.IsUnique(true);
		}
	}
}
