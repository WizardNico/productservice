﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ProductService.Data.Migrations
{
	public partial class First : Migration
	{
		protected override void Up(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.CreateTable(
				name: "Categories",
				columns: table => new
				{
					Id = table.Column<long>(nullable: false)
						.Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
					Code = table.Column<string>(maxLength: 10, nullable: false),
					Description = table.Column<string>(maxLength: 100, nullable: false)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_Categories", x => x.Id);
				});

			migrationBuilder.CreateTable(
				name: "Options",
				columns: table => new
				{
					Id = table.Column<long>(nullable: false)
						.Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
					AdditionalPrice = table.Column<double>(nullable: false),
					Code = table.Column<string>(maxLength: 10, nullable: false),
					Description = table.Column<string>(maxLength: 100, nullable: false)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_Options", x => x.Id);
				});

			migrationBuilder.CreateTable(
				name: "Products",
				columns: table => new
				{
					Id = table.Column<long>(nullable: false)
						.Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
					Availability = table.Column<int>(nullable: false),
					CategoryId = table.Column<long>(nullable: false),
					Code = table.Column<string>(maxLength: 10, nullable: false),
					Description = table.Column<string>(maxLength: 100, nullable: false),
					InsertionDate = table.Column<DateTime>(nullable: false),
					Price = table.Column<double>(nullable: false),
					UpdateDate = table.Column<DateTime>(nullable: false),
					Vat = table.Column<double>(nullable: false)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_Products", x => x.Id);
					table.ForeignKey(
						name: "FK_Products_Categories_CategoryId",
						column: x => x.CategoryId,
						principalTable: "Categories",
						principalColumn: "Id",
						onDelete: ReferentialAction.Cascade);
				});

			migrationBuilder.CreateTable(
				name: "OptionProducts",
				columns: table => new
				{
					Id = table.Column<long>(nullable: false)
						.Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
					OptionId = table.Column<long>(nullable: false),
					ProductId = table.Column<long>(nullable: false)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_OptionProducts", x => x.Id);
					table.ForeignKey(
						name: "FK_OptionProducts_Options_OptionId",
						column: x => x.OptionId,
						principalTable: "Options",
						principalColumn: "Id",
						onDelete: ReferentialAction.Cascade);
					table.ForeignKey(
						name: "FK_OptionProducts_Products_ProductId",
						column: x => x.ProductId,
						principalTable: "Products",
						principalColumn: "Id",
						onDelete: ReferentialAction.Cascade);
				});

			migrationBuilder.CreateIndex(
				name: "IX_Categories_Code",
				table: "Categories",
				column: "Code",
				unique: true);

			migrationBuilder.CreateIndex(
				name: "IX_Options_Code",
				table: "Options",
				column: "Code",
				unique: true);

			migrationBuilder.CreateIndex(
				name: "IX_OptionProducts_ProductId",
				table: "OptionProducts",
				column: "ProductId");

			migrationBuilder.CreateIndex(
				name: "IX_OptionProducts_OptionId_ProductId",
				table: "OptionProducts",
				columns: new[] { "OptionId", "ProductId" },
				unique: true);

			migrationBuilder.CreateIndex(
				name: "IX_Products_CategoryId",
				table: "Products",
				column: "CategoryId");

			migrationBuilder.CreateIndex(
				name: "IX_Products_Code",
				table: "Products",
				column: "Code",
				unique: true);
		}

		protected override void Down(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.DropTable(
				name: "OptionProducts");

			migrationBuilder.DropTable(
				name: "Options");

			migrationBuilder.DropTable(
				name: "Products");

			migrationBuilder.DropTable(
				name: "Categories");
		}
	}
}
