﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProductService.Data.Migrations
{
	public partial class RunSqlScript : Migration
	{
		protected override void Up(MigrationBuilder migrationBuilder)
		{
			//Inserisco le opzioni sul prodotto
			migrationBuilder.Sql("INSERT INTO Options (Code, Description, AdditionalPrice) VALUES ('OPT1','Description first option', 10)");
			migrationBuilder.Sql("INSERT INTO Options (Code, Description, AdditionalPrice) VALUES ('OPT2','Description second option', 7)");
			migrationBuilder.Sql("INSERT INTO Options (Code, Description, AdditionalPrice) VALUES ('OPT3','Description third option', 2)");
			migrationBuilder.Sql("INSERT INTO Options (Code, Description, AdditionalPrice) VALUES ('OPT4','Description fourth option', 5)");


			//Inserisco le categorie
			migrationBuilder.Sql("INSERT INTO Categories (Code, Description) VALUES ('CAT_A','Description for category A')");
			migrationBuilder.Sql("INSERT INTO Categories (Code, Description) VALUES ('CAT_B','Description for category B')");
			migrationBuilder.Sql("INSERT INTO Categories (Code, Description) VALUES ('CAT_C','Description for category C')");
			migrationBuilder.Sql("INSERT INTO Categories (Code, Description) VALUES ('CAT_D','Description for category D')");
			migrationBuilder.Sql("INSERT INTO Categories (Code, Description) VALUES ('CAT_E','Description for category E')");


			//Inserisco i prodotti
			migrationBuilder.Sql("INSERT INTO Products (Code, Description, InsertionDate, UpdateDate, Price, Vat, Availability, CategoryId) VALUES ('PRD_1','Description for product 1', GETDATE(), GETDATE(), 10, 22, 100000, 1)");
			migrationBuilder.Sql("INSERT INTO Products (Code, Description, InsertionDate, UpdateDate, Price, Vat, Availability, CategoryId) VALUES ('PRD_2','Description for product 2', GETDATE(), GETDATE(), 5, 22, 100000, 2)");
			migrationBuilder.Sql("INSERT INTO Products (Code, Description, InsertionDate, UpdateDate, Price, Vat, Availability, CategoryId) VALUES ('PRD_3','Description for product 3', GETDATE(), GETDATE(), 20, 7, 100000, 3)");
			migrationBuilder.Sql("INSERT INTO Products (Code, Description, InsertionDate, UpdateDate, Price, Vat, Availability, CategoryId) VALUES ('PRD_4','Description for product 4', GETDATE(), GETDATE(), 10, 22, 100000, 1)");
			migrationBuilder.Sql("INSERT INTO Products (Code, Description, InsertionDate, UpdateDate, Price, Vat, Availability, CategoryId) VALUES ('PRD_5','Description for product 5', GETDATE(), GETDATE(), 5.5, 22, 100000, 1)");
			migrationBuilder.Sql("INSERT INTO Products (Code, Description, InsertionDate, UpdateDate, Price, Vat, Availability, CategoryId) VALUES ('PRD_6','Description for product 6', GETDATE(), GETDATE(), 10, 7, 100000, 3)");
			migrationBuilder.Sql("INSERT INTO Products (Code, Description, InsertionDate, UpdateDate, Price, Vat, Availability, CategoryId) VALUES ('PRD_7','Description for product 7', GETDATE(), GETDATE(), 50, 22, 100000, 3)");
			migrationBuilder.Sql("INSERT INTO Products (Code, Description, InsertionDate, UpdateDate, Price, Vat, Availability, CategoryId) VALUES ('PRD_8','Description for product 8', GETDATE(), GETDATE(), 10.5, 22, 100000, 1)");
			migrationBuilder.Sql("INSERT INTO Products (Code, Description, InsertionDate, UpdateDate, Price, Vat, Availability, CategoryId) VALUES ('PRD_9','Description for product 9', GETDATE(), GETDATE(), 30, 7, 100000, 2)");
			migrationBuilder.Sql("INSERT INTO Products (Code, Description, InsertionDate, UpdateDate, Price, Vat, Availability, CategoryId) VALUES ('PRD_10','Description for product 10', GETDATE(), GETDATE(), 10, 22, 100000, 4)");


			//Collego le opzioni con i prodotti

			//Prodotto uno con tutte le opzioni
			migrationBuilder.Sql("INSERT INTO OptionProducts (OptionId, ProductId) VALUES (1, 1)");
			migrationBuilder.Sql("INSERT INTO OptionProducts (OptionId, ProductId) VALUES (2, 1)");
			migrationBuilder.Sql("INSERT INTO OptionProducts (OptionId, ProductId) VALUES (3, 1)");
			migrationBuilder.Sql("INSERT INTO OptionProducts (OptionId, ProductId) VALUES (4, 1)");

			//Prodotto due con tutte le opzioni
			migrationBuilder.Sql("INSERT INTO OptionProducts (OptionId, ProductId) VALUES (1, 2)");
			migrationBuilder.Sql("INSERT INTO OptionProducts (OptionId, ProductId) VALUES (2, 2)");
			migrationBuilder.Sql("INSERT INTO OptionProducts (OptionId, ProductId) VALUES (3, 2)");
			migrationBuilder.Sql("INSERT INTO OptionProducts (OptionId, ProductId) VALUES (4, 2)");

			//Prodotto tre con tutte le opzioni
			migrationBuilder.Sql("INSERT INTO OptionProducts (OptionId, ProductId) VALUES (1, 3)");
			migrationBuilder.Sql("INSERT INTO OptionProducts (OptionId, ProductId) VALUES (2, 3)");
			migrationBuilder.Sql("INSERT INTO OptionProducts (OptionId, ProductId) VALUES (3, 3)");
			migrationBuilder.Sql("INSERT INTO OptionProducts (OptionId, ProductId) VALUES (4, 3)");

			//Prodotto quattro con 2 opzioni
			migrationBuilder.Sql("INSERT INTO OptionProducts (OptionId, ProductId) VALUES (1, 4)");
			migrationBuilder.Sql("INSERT INTO OptionProducts (OptionId, ProductId) VALUES (3, 4)");

			//Prodotto cinque con 2 opzioni
			migrationBuilder.Sql("INSERT INTO OptionProducts (OptionId, ProductId) VALUES (2, 5)");
			migrationBuilder.Sql("INSERT INTO OptionProducts (OptionId, ProductId) VALUES (4, 5)");

			//Prodotto sei con 3 opzioni
			migrationBuilder.Sql("INSERT INTO OptionProducts (OptionId, ProductId) VALUES (2, 6)");
			migrationBuilder.Sql("INSERT INTO OptionProducts (OptionId, ProductId) VALUES (3, 6)");
			migrationBuilder.Sql("INSERT INTO OptionProducts (OptionId, ProductId) VALUES (4, 6)");

			//Prodotto sette con 3 opzioni
			migrationBuilder.Sql("INSERT INTO OptionProducts (OptionId, ProductId) VALUES (1, 7)");
			migrationBuilder.Sql("INSERT INTO OptionProducts (OptionId, ProductId) VALUES (2, 7)");
			migrationBuilder.Sql("INSERT INTO OptionProducts (OptionId, ProductId) VALUES (3, 7)");

			//Prodotto otto con 1 opzione
			migrationBuilder.Sql("INSERT INTO OptionProducts (OptionId, ProductId) VALUES (1, 8)");

			//Prodotto nove con 1 opzione
			migrationBuilder.Sql("INSERT INTO OptionProducts (OptionId, ProductId) VALUES (4, 9)");
		}

		protected override void Down(MigrationBuilder migrationBuilder)
		{

		}
	}
}
