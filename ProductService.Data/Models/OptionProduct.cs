﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProductService.Data.Models
{
	public class OptionProduct
	{
		public long Id { get; set; }



		public long OptionId { get; set; }
		public Option Option { get; set; }

		public long ProductId { get; set; }
		public Product Product { get; set; }
	}
}
