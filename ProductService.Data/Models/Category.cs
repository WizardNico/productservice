﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProductService.Data.Models
{
    public class Category
    {
		public long Id { get; set; }



		[Required]
		[MinLength(3)]
		[MaxLength(10)]
		public string Code { get; set; }

		[Required]
		[MinLength(5)]
		[MaxLength(100)]
		public string Description { get; set; }



		public List<Product> Products { get; set; }
    }
}
