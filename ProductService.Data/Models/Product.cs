﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace ProductService.Data.Models
{
	public class Product
	{
		public long Id { get; set; }



		[Required]
		[MinLength(3)]
		[MaxLength(10)]
		public string Code { get; set; }

		[Required]
		[MinLength(5)]
		[MaxLength(100)]
		public string Description { get; set; }

		public DateTime InsertionDate { get; set; }

		public DateTime UpdateDate { get; set; }

		public double Price { get; set; }

		public double Vat { get; set; }

		public int Availability { get; set; }



		public long CategoryId { get; set; }
		public Category Category { get; set; }



		public List<OptionProduct> OptionProducts { get; set; }
	}
}
