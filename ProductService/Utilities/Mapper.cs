﻿using ProductService.Directories.Ecommerce.Products;
using ProductService.Data.Models;
using System.Collections.Generic;
using EcommerceProductModels = ProductService.Directories.Ecommerce.Products;
using DataModels = ProductService.Data.Models;
using ProductService.Directories.Ecommerce.Products.Shared;

namespace ProductService.Utilities
{
	public class Mapper
	{
		public static EcommerceProductModels.Product Map(DataModels.Product product)
		{
			List<ProductOption> list = new List<ProductOption>();
			if (product.OptionProducts != null)
				product.OptionProducts.ForEach(oopt => list.Add(new ProductOption() { AdditionalPrice = oopt.Option.AdditionalPrice, Code = oopt.Option.Code, }));

			return new EcommerceProductModels.Product()
			{
				Movements = 0,
				Availability = product.Availability,
				CategoryCode = product.Category.Code,
				Code = product.Code,
				Description = product.Description,
				Price = product.Price,
				Vat = product.Vat,
				Options = list,
			};
		}
	}
}
