﻿using AgileMQ.Containers;
using AgileMQ.DTO;
using AgileMQ.Enums;
using AgileMQ.Extensions;
using AgileMQ.Interfaces;
using ProductService.Data;
using ProductService.Data.Exceptions;
using ProductService.Data.Repositories;
using ProductService.Directories.Ecommerce.Orders.Models;
using ProductService.Directories.Ecommerce.Products;
using ProductService.Directories.Ecommerce.Products.Shared;
using ProductService.Utilities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataModels = ProductService.Data.Models;

namespace ProductService.Subscribers
{
	public class ProductSubscriber : IAgileSubscriber<Product>
	{
		public IAgileBus Bus { get; set; }

		public async Task<Product> GetAsync(Product model, MessageContainer container)
		{
			DataContext ctx = container.Resolve<DataContext>();

			DataModels.Product product = await ctx.Products.FindByCodeAsync(model.Code);

			if (product == null)
				throw new ObjectNotFoundException("Product not found, a wrong code was used.");

			model = Mapper.Map(product);

			return (model);
		}

		public async Task<Product> PostAsync(Product model, MessageContainer container)
		{
			DataContext ctx = container.Resolve<DataContext>();

			//Prendo la categoria
			DataModels.Category category = await ctx.Categories.FindCategoryByCodeAsync(model.CategoryCode);

			if (category == null)
				throw new ObjectNotFoundException("Category not found, a wrong code was used.");

			//Creo il prodotto
			DataModels.Product product = new DataModels.Product()
			{
				Availability = model.Availability.Value,
				Code = model.Code,
				Description = model.Description,
				InsertionDate = DateTime.Now,
				Price = model.Price.Value,
				Vat = model.Vat.Value,
				UpdateDate = DateTime.Now,
				Category = category,
				CategoryId = category.Id,
				OptionProducts = new List<DataModels.OptionProduct>()
			};

			//Aggiungo il prodotto
			await ctx.Products.AddAsync(product);

			//Per ogni opzione attiva sul prodotto, la aggiungo in relazione al prodotto stesso
			foreach (ProductOption opt in model.Options)
			{
				DataModels.Option option = await ctx.Options.FindOptionByCodeAsync(opt.Code);

				if (option == null)
				{
					throw new ObjectNotFoundException("Option not found, a wrong code was used.");
				} else
				{
					DataModels.OptionProduct optPrd = new DataModels.OptionProduct()
					{
						Option = option,
						OptionId = option.Id,
						Product = product,
						ProductId = product.Id,
					};
					product.OptionProducts.Add(optPrd);

					await ctx.OptionProducts.AddAsync(optPrd);
				}
			}

			await ctx.Products.AddAsync(product);
			await ctx.SaveChangesAsync();

			model = Mapper.Map(product);

			return (model);
		}

		public async Task PutAsync(Product model, MessageContainer container)
		{
			DataContext ctx = container.Resolve<DataContext>();

			//Prendo la categoria
			DataModels.Category category = await ctx.Categories.FindCategoryByCodeAsync(model.CategoryCode);

			if (category == null)
				throw new ObjectNotFoundException("Category not found, a wrong code was used.");

			//Prendo il prodotto
			DataModels.Product product = await ctx.Products.FindByCodeAsync(model.Code);

			if (product == null)
				throw new ObjectNotFoundException("Product not found, a wrong code was used.");

			//Modifico il prodotto
			product.Category = category;
			product.Description = model.Description;
			product.Code = model.Code;
			product.Price = model.Price.Value;
			product.UpdateDate = DateTime.Now;
			product.Vat = model.Vat.Value;
			product.Availability += model.Movements.Value;

			//Come prima cosa elimino tutte le opzioni sul prodotto
			foreach (DataModels.OptionProduct optPrd in ctx.OptionProducts)
			{
				if (optPrd.ProductId == product.Id)
				{
					ctx.OptionProducts.Remove(optPrd);
				}
			}

			//Vado ad inserire solo le opzioni attive
			foreach (ProductOption opt in model.Options)
			{
				DataModels.Option option = await ctx.Options.FindOptionByCodeAsync(opt.Code);

				if (option == null)
				{
					throw new ObjectNotFoundException("Option not found, a wrong code was used.");
				} else
				{
					DataModels.OptionProduct optPrd = new DataModels.OptionProduct()
					{
						Option = option,
						Product = product
					};

					await ctx.OptionProducts.AddAsync(optPrd);
				}
			}

			await ctx.SaveChangesAsync();
		}

		public async Task DeleteAsync(Product model, MessageContainer container)
		{
			DataContext ctx = container.Resolve<DataContext>();

			DataModels.Product product = await ctx.Products.FindByCodeAsync(model.Code);

			if (product == null)
				throw new ObjectNotFoundException("Product not found, a wrong code was used.");

			foreach (DataModels.OptionProduct optPrd in ctx.OptionProducts)
			{
				if (optPrd.ProductId == product.Id)
				{
					ctx.OptionProducts.Remove(optPrd);
				}
			}

			ctx.Products.Remove(product);
			await ctx.SaveChangesAsync();
		}

		public async Task<Page<Product>> GetPagedListAsync(Dictionary<string, object> filter, long pageIndex, int pageSize, MessageContainer container)
		{
			DataContext ctx = container.Resolve<DataContext>();

			string categoryCode = filter.Get<string>("category");

			Page<Product> page = new Page<Product>()
			{
				Items = new List<Product>()
			};
			List<DataModels.Product> productList = await ctx.Products.GetListAsync(categoryCode, pageSize, pageIndex);

			foreach (DataModels.Product product in productList)
			{
				page.Items.Add(Mapper.Map(product));
			}

			page.TotalItemCount = await ctx.Products.CountAsync(categoryCode);
			page.PageCount = (int)page.TotalItemCount / pageSize;

			return (page);
		}

		public Task<List<Product>> GetListAsync(Dictionary<string, object> filter, MessageContainer container)
		{
			throw new NotImplementedException();
		}

		public async Task PurchasedAsync(Product model, MessageContainer container)
		{
			DataContext ctx = container.Resolve<DataContext>();

			//Prendo il prodotto
			DataModels.Product product = await ctx.Products.FindByCodeAsync(model.Code);

			//Scalo la quantità
			product.Availability -= model.Movements.Value;

			await ctx.SaveChangesAsync();
		}

	}
}