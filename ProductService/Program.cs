﻿using AgileMQ.Bus;
using AgileMQ.Enums;
using AgileMQ.Interfaces;
using ProductService.Data;
using ProductService.Data.Models;
using ProductService.Loggers;
using ProductService.Subscribers;
using System;

namespace ProductService
{
	public class Program
	{
		public static void Main(string[] args)
		{
			using (IAgileBus bus = new RabbitMQBus("HostName=localhost;Port=5672;UserName=guest;Password=guest;Timeout=3;RetryLimit=3;PrefetchCount=100;AppId=ProductService"))
			{
				bus.Logger = new ConsoleLogger();

				bus.Container.Register<DataContext, DataContext>(InjectionScope.Message);

				bus.Suscribe(new ProductSubscriber());

				Console.WriteLine("ProductService Ready!");
				Console.ReadLine();
			}
		}
	}
}