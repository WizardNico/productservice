﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ProductService.Directories.Ecommerce.Products.Shared
{
	public class ProductOption
	{
		[Required]
		[MinLength(3)]
		[MaxLength(10)]
		public string Code { get; set; }

		[Required]
		public double? AdditionalPrice { get; set; }
	}
}
